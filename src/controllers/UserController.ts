import {Request, Response} from "express"
import IController from "./ControllerInterface"

let data: any[] = [
    {id: 1, name: "Adi"},
    {id: 2, name: "Budi"},
    {id: 3, name: "Cici"},
    {id: 4, name: "Didi"},
]

class UserController implements IController {
    index(req: Request, res: Response): Response {
        return res.send(data)
    }
    create(req: Request, res: Response): Response {
        const {id, name} = req.body

        data.push({id, name})
        return res.send("Create Success")
    }
    show(req: Request, res: Response): Response {
        const id = Number(req.params.id)

        const person = data.find(x => x.id === id)
        return res.send(person)
    }
    update(req: Request, res: Response): Response {
        const id = Number(req.params.id)

        const person = data.find(x => x.id === id)
        person.name = req.body.name
        return res.send("Update Success")
    }
    delete(req: Request, res: Response): Response {
        const id = Number(req.params.id)

        const indexPerson = data.findIndex(x => x.id === id)
        data.splice(indexPerson, 1)

        return res.send("Delete Success")
    }
}

export default new UserController()